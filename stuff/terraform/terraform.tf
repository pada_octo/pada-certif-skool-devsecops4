# terraform.tf
terraform {

  required_providers {
    aws = {
      version = "~> 2.0"
    }
  }
}

provider "aws" {
  region  = "eu-west-1"
}
