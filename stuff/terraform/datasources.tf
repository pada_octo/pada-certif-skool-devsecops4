# datasources.tf
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-*-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "template_file" "user_data" {
  template = file("cloud-init.yml")
  vars = {
    ssh_port = var.ssh_port
  }
}

data "aws_route53_zone" "route53_domain" {
  name = "${var.domain}."
}
