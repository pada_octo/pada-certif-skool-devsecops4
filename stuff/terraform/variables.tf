# variables.tf
variable "trigramme" {
  description = "The trigramme of the user."
  type        = string
  default     = "pada"
}

variable "ssh_port" {
  description = "The SSH Port"
  type        = number
  default     = 20022
}

variable "domain" {
  description = "The DNS Route53 Domain to use."
  type        = string
  default     = "aws.octo.training"
}

variable "pub_key" {
  description = "The SSH pub key to install"
  type        = string
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC2MEzirzFkv52mO35qD9y6QgliQ8dD1gywc4UPHXozQVivltkOhw9xvxavyN25xY4n90AEfqg2NPioai2QHPZlFZ9o+s7ES000H7SbVz7jNn0y1WhXXYyYs2DL3voexiYT4zAewiG2ac3Dhwio3X/jFgYGoXX7lMS9pn6xNbOc7FxH0YWXBYzE7pjyGXKl8ap8ltYxMfouQkNBtar39k9YG0xwTGWLgaWexuidkuxyeYGlNFzC7tTxAD3ZWSn1eyqYwCopvqwaV5Dn8UMrEEB76eH0PKOmasZGhw9oMWpTsFFTq9gnLGfJS9iQsIkSg6iBm1ZJhH56SyAdIfiPBpKP"
}
