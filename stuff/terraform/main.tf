# main.tf

resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    "Trigramme" = var.trigramme
    "Maker"     = var.trigramme
    "Training"  = "devsecops"
    "Name"      = "my_vpc-${var.trigramme}"
  }
}

resource "aws_internet_gateway" "my_ig" {

  vpc_id = aws_vpc.my_vpc.id
  tags = {
    "Trigramme" = var.trigramme
    "Maker"     = var.trigramme
    "Training"  = "devsecops"
    "Name"      = "my_ig-${var.trigramme}"
  }
}

resource "aws_route_table" "my_rt" {
  vpc_id = aws_vpc.my_vpc.id
  tags = {
    "Trigramme" = var.trigramme
    "Maker"     = var.trigramme
    "Training"  = "devsecops"
    "Name"      = "my_rt-${var.trigramme}"
  }
}

resource "aws_route" "my_route_public_internet_gateway" {
  route_table_id         = aws_route_table.my_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.my_ig.id
}

resource "aws_subnet" "my_subnet_a" {
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "eu-west-1a"
  tags = {
    "Trigramme" = var.trigramme
    "Maker"     = var.trigramme
    "Training"  = "devsecops"
    "Name"      = "my_subnet_a-${var.trigramme}"
  }
}

resource "aws_subnet" "my_subnet_b" {
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "eu-west-1b"
  tags = {
    "Trigramme" = var.trigramme
    "Maker"     = var.trigramme
    "Training"  = "devsecops"
    "Name"      = "my_subnet_b-${var.trigramme}"
  }
}

resource "aws_route_table_association" "public_a" {
  subnet_id      = aws_subnet.my_subnet_a.id
  route_table_id = aws_route_table.my_rt.id
}

resource "aws_route_table_association" "public_b" {
  subnet_id      = aws_subnet.my_subnet_b.id
  route_table_id = aws_route_table.my_rt.id
}

resource "aws_security_group" "default_sg" {
  name   = "devsecops-default-${var.trigramme}"
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    "Trigramme" = var.trigramme
    "Maker"     = var.trigramme
    "Training"  = "devsecops"
    "Name"      = "default-sg-${var.trigramme}"
  }
}

resource "aws_security_group_rule" "ssh_ingress" {
  from_port         = var.ssh_port
  protocol          = "tcp"
  security_group_id = aws_security_group.default_sg.id
  to_port           = var.ssh_port
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "http_ingress" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.default_sg.id
  to_port           = 80
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "https_ingress" {
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.default_sg.id
  to_port           = 443
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "https_egress" {
  from_port         = "443"
  protocol          = "tcp"
  security_group_id = aws_security_group.default_sg.id
  to_port           = "443"
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "http_egress" {
  from_port         = "80"
  protocol          = "tcp"
  security_group_id = aws_security_group.default_sg.id
  to_port           = "80"
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "all_egress_to_current" {
  from_port         = "0"
  protocol          = "tcp"
  security_group_id = aws_security_group.default_sg.id
  to_port           = "65535"
  type              = "egress"
  self = true
}
resource "aws_security_group_rule" "all_ingress_to_current" {
  from_port         = "0"
  protocol          = "tcp"
  security_group_id = aws_security_group.default_sg.id
  to_port           = "65535"
  type              = "ingress"
  self = true
}

resource "aws_security_group" "vpc_sg" {
  name   = "devsecops-vpc-${var.trigramme}"
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    "Trigramme" = var.trigramme
    "Maker"     = var.trigramme
    "Training"  = "devsecops"
    "Name"      = "vpc-sg-${var.trigramme}"
  }
}

resource "aws_security_group_rule" "vpc_ssh_ingress" {
  from_port         = var.ssh_port
  protocol          = "tcp"
  security_group_id = aws_security_group.vpc_sg.id
  to_port           = var.ssh_port
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "vpc_http_egress" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.vpc_sg.id
  to_port           = 80
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "vpc_https_egress" {
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.vpc_sg.id
  to_port           = 443
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_key_pair" "deployer" {
  key_name   = "devsecops-exam-${var.trigramme}"
  public_key = var.pub_key
}

resource "aws_instance" "m1" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.my_subnet_a.id

  key_name = "devsecops-exam-${var.trigramme}"

  associate_public_ip_address = true

  vpc_security_group_ids = [aws_security_group.default_sg.id]

  tags = {
    "Trigramme" = var.trigramme
    "Maker"     = var.trigramme
    "Training"  = "devsecops"
    "Role"      = "rp"
    "Name"      = "m1-${var.trigramme}"
  }
  # Cloudinit
  user_data = data.template_file.user_data.rendered
}

resource "aws_instance" "m2" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.my_subnet_b.id

  key_name = "devsecops-exam-${var.trigramme}"

  associate_public_ip_address = true

  vpc_security_group_ids = [aws_security_group.default_sg.id]

  tags = {
    "Trigramme" = var.trigramme
    "Maker"     = var.trigramme
    "Training"  = "devsecops"
    "Role"      = "rp"
    "Name"      = "m2-${var.trigramme}"
  }
  # Cloudinit
  user_data = data.template_file.user_data.rendered
}

resource "aws_instance" "monit" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.my_subnet_a.id

  key_name = "devsecops-exam-${var.trigramme}"

  associate_public_ip_address = true

  vpc_security_group_ids = [aws_security_group.default_sg.id]

  tags = {
    "Trigramme" = var.trigramme
    "Maker"     = var.trigramme
    "Training"  = "devsecops"
    "Role"      = "monitoring"
    "Name"      = "monitoring-${var.trigramme}"
  }

  # Cloudinit
  user_data = data.template_file.user_data.rendered
}

resource "aws_lb" "my_alb" {
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.default_sg.id]
  subnets            = [aws_subnet.my_subnet_a.id, aws_subnet.my_subnet_b.id]

  tags = {
    "Trigramme" = var.trigramme
    "Maker"     = var.trigramme
    "Training"  = "devsecops"
    "Name"      = "my-alb-${var.trigramme}"
  }
}

resource "aws_lb_target_group" "my_alb_tg" {
  name     = "my-alb-tg-${var.trigramme}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.my_vpc.id
}

resource "aws_lb_target_group_attachment" "my_alb_tga" {
  target_group_arn = aws_lb_target_group.my_alb_tg.arn
  target_id        = aws_instance.m1.id
  port             = 80
}

resource "aws_lb_listener" "my_alb_listener" {
  load_balancer_arn = aws_lb.my_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my_alb_tg.arn
  }
}

resource "aws_route53_record" "dns_record" {
  zone_id = data.aws_route53_zone.route53_domain.zone_id
  name    = "*.${var.trigramme}.${data.aws_route53_zone.route53_domain.name}"
  type    = "A"
  alias {
    name                   = aws_lb.my_alb.dns_name
    zone_id                = aws_lb.my_alb.zone_id
    evaluate_target_health = true
  }
}

